package id.ac.ub.fbdb1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    Button btSimpan;
    EditText etJudul;
    EditText etIsi;
Button btLis;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btSimpan = findViewById(R.id.btSimpan);
        etJudul = findViewById(R.id.etJudul);
        etIsi = findViewById(R.id.etIsi);
        btLis=findViewById(R.id.btList);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference();
        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference postReference = dbRef.child("posts");
                String judul = etJudul.getText().toString();
                String isi = etIsi.getText().toString();
                Post post = new Post();
                post.setJudul(judul);
                post.setIsi(isi);
                String key=postReference.push().getKey();
                post.setId(key);
                postReference.child(key).setValue(post);
                Intent i=new Intent(getApplicationContext(),ListActivity.class);
                startActivity(i);
            }
        });

        btLis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),ListActivity.class);
                startActivity(i);
            }
        });
    }
}